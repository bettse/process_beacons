const msgpack = require('msgpack5')()
const mqtt = require('mqtt')
const Influx = require('influx')
const advlib = require('advlib');
const os = require('os')

const influx = new Influx.InfluxDB('http://10.0.1.200:8086/telegraf')

const opts = {
  port: 18002,
  username: 'bettse',
  password: 'bettse'
}
const client  = mqtt.connect('mqtt://postman.cloudmqtt.com', opts);
const topic = 'beacons'

client.on('connect', function () {
  console.log("mqtt connected")
  client.subscribe(topic)
})

client.on('message', function (topic, message) {
  var data;
  try {
    data = msgpack.decode(message);
  } catch(err) {
    console.log(err)
    return;
  }
  if (typeof data.devices == "undefined") {
    return;
  }
  const { v, mid, time, ip, mac: gatewayMac, devices } = data
  // console.log({time, count: devices.length});
  const metrics = devices.map((device) => {
    const type = device[0]
    const mac = device.slice(1, 7).toString('hex')
    const rssi = device[7] - 256
    const advertisement = device.slice(8)
    const metric = {
      measurement: 'ble',
      tags: {
        host: os.hostname(),
        mac,
        gateway_mac: gatewayMac.toString('hex').toLowerCase(),
        gateway_ip: ip,
      },
      fields: { rssi }
    }
    try {
      const parsed = advlib.ble.data.process(advertisement.toString('hex'))
      const { completeLocalName, manufacturerSpecificData } = parsed
      if (completeLocalName) {
        metric.tags.localName = completeLocalName
      }
      if (manufacturerSpecificData) {
        const { companyName } = manufacturerSpecificData
        if (companyName) {
          metric.tags.companyName = companyName
        }
      }
    } catch (err) {
      console.log(err)
    }
    // console.log({mac, rssi})
    return metric
  })
  return influx.writePoints(metrics)
})

